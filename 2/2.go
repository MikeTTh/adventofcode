package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type line struct {
	Min int
	Max int
	Char int32
	Pass string
}

func (l *line) Validate1() bool {
	amount := 0
	for _, c := range l.Pass {
		if c == l.Char {
			amount++
		}
	}

	return amount >= l.Min && amount <= l.Max
}

func (l *line) Validate2() bool {
	first := l.Pass[l.Min-1] == uint8(l.Char)
	last := l.Pass[l.Max-1] == uint8(l.Char)
	return first != last
}

func main() {
	str, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}
	
	good1 := 0
	good2 := 0
	
	lines := strings.Split(string(str), "\n")
	
	for _, s := range lines {
		l := line{}
		parts := strings.Split(s, " ")

		minMax := strings.Split(parts[0], "-")
		l.Min, e = strconv.Atoi(minMax[0])
		l.Max, e = strconv.Atoi(minMax[1])

		l.Char = int32(parts[1][0])

		l.Pass = parts[2]
		
		if l.Validate1() {
			good1++
		}
		
		if l.Validate2() {
			good2++
		}
	}
	
	fmt.Printf("Good passwords according to first policy: %d\n", good1)
	fmt.Printf("Good passwords according to second policy: %d\n", good2)
}
