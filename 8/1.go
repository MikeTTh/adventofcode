package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	b, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}

	lines := strings.Split(string(b), "\n")

	acc := 0
	ic := 0

	ran := make(map[int]bool)

	for {
		if ran[ic] {
			break
		}

		words := strings.Split(lines[ic], " ")
		instr := words[0]
		arg := words[1]

		a, e := strconv.Atoi(arg)
		if e != nil {
			panic(e)
		}

		ran[ic] = true

		switch instr {
		case "nop":
			ic++
		case "jmp":
			ic += a
		case "acc":
			acc += a
			ic++
		}
	}

	fmt.Printf("acc=%d\n", acc)
}
