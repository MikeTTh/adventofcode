package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	b, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}

	origlines := strings.Split(string(b), "\n")

	acc := 0
	ic := 0
	correct := false
	toReplace := -1

	wasReplaced := make(map[int]bool)

	for !correct {
		acc = 0
		ic = 0
		longestwegot := 0
		ran := make(map[int]bool)
		lines := make([]string, len(origlines))
		copy(lines, origlines)

		if toReplace != -1 {
			if lines[toReplace][:3] == "nop" {
				lines[toReplace] = strings.ReplaceAll(lines[toReplace], "nop", "jmp")
			} else {
				lines[toReplace] = strings.ReplaceAll(lines[toReplace], "jmp", "nop")
			}
			wasReplaced[toReplace] = true
		}

		for {
			if ic >= len(lines) {
				correct = true
				break
			}

			if ran[ic] {
				break
			}

			if ic >= longestwegot {
				longestwegot = ic
			}

			words := strings.Split(lines[ic], " ")
			instr := words[0]
			arg := words[1]

			a, e := strconv.Atoi(arg)
			if e != nil {
				panic(e)
			}

			ran[ic] = true

			switch instr {
			case "nop":
				if !wasReplaced[ic] {
					toReplace = ic
					break
				}
				ic++
			case "jmp":
				if !wasReplaced[ic] {
					toReplace = ic
				}
				ic += a
			case "acc":
				acc += a
				ic++
			}
		}

		fmt.Println("Got to", longestwegot, "trying to replace", lines[toReplace][:3], "@", toReplace)
	}

	fmt.Printf("acc=%d\n", acc)
}
