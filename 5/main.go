package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

// F = low bit
// B = high bit
// 7 bit number

// R = high bit
// L = low bit
// 3 bit number

type seat struct {
	Row int
	Col int
	ID  int
}

func decode(in string) seat {
	if len(in) < 10 {
		return seat{-1, -1, -1}
	}

	rowStr := in[:7]
	colStr := in[7:]

	rowStr = strings.ReplaceAll(rowStr, "F", "0")
	rowStr = strings.ReplaceAll(rowStr, "B", "1")

	colStr = strings.ReplaceAll(colStr, "L", "0")
	colStr = strings.ReplaceAll(colStr, "R", "1")

	row, e := strconv.ParseInt(rowStr, 2, 32)
	if e != nil {
		return seat{-1, -1, -1}
	}

	col, e := strconv.ParseInt(colStr, 2, 32)
	if e != nil {
		return seat{-1, -1, -1}
	}

	return seat{int(row), int(col), int(row*8 + col)}
}

func main() {
	b, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}

	rows := strings.Split(string(b), "\n")

	seats := make([]seat, len(rows))

	for i, r := range rows {
		s := decode(r)
		seats[i] = s
	}

	sort.Slice(seats, func(i, j int) bool {
		return seats[i].ID < seats[j].ID
	})

	fmt.Printf("highest id: %d\n", seats[len(seats)-1].ID)

	prevID := -1
	for _, s := range seats {
		if prevID == -1 {
			prevID = s.ID
			continue
		}

		if s.ID != prevID+1 {
			fmt.Printf("our seat id is: %d\n", s.ID-1)
			break
		}

		prevID = s.ID
	}
}
