package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

var prev leaderboard

const filename = "data/prev.json"

func init() {
	if by, e := ioutil.ReadFile(filename); e == nil {
		_ = json.Unmarshal(by, &prev)
	}
}

func save() {
	if by, e := json.Marshal(prev); e == nil {
		_ = os.Mkdir("data", os.ModePerm)
		_ = ioutil.WriteFile(filename, by, os.ModePerm)
	}
}

func spaceify(s string) string {
	magic := '​'
	s2 := make([]rune, 0, len(s)*2)
	for _, r := range []rune(s) {
		s2 = append(s2, r)
		s2 = append(s2, magic)
	}
	return string(s2)
}

func main() {
	for {
		lead, e := getLeaderboard()
		if e != nil {
			fmt.Println("couldn't get leaderboard", e)
			return
		}

		if d := lead.Diff(prev); d != "" {
			if len(prev) == 0 {
				d = "Üdv!"
			}
			msg := fmt.Sprintf("%s\n"+
				"A jelenlegi állás:\n\n"+
				"| Név | Csillagok | Pontok |\n"+
				"| --- | :-------: | -----: |\n", spaceify(d))

			for _, p := range lead {
				msg += fmt.Sprintf("| %s | %d | %d |\n", spaceify(p.Name), p.Stars, p.Points)
			}

			e = send(msg)
			if e != nil {
				fmt.Println("couldn't send leaderboard", e)
			}
		}

		prev = lead
		save()
		time.Sleep(time.Minute * 15)
	}

}
