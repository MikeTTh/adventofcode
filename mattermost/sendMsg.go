package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"os"
)

var secret = ""

func init() {
	if env := os.Getenv("MM_SECRET"); env != "" {
		secret = env
	} else {
		panic(errors.New("missing env variable MM_SECRET"))
	}

}

type mmData struct {
	Text string `json:"text"`
}

func send(msg string) error {
	url := "https://mattermost.kszk.bme.hu/hooks/" + secret

	m := mmData{
		Text: msg,
	}

	by, e := json.Marshal(m)
	if e != nil {
		return e
	}

	var buf bytes.Buffer
	buf.Write(by)

	_, e = http.Post(url, "application/json", &buf)
	if e != nil {
		return e
	}

	return nil
}
