package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"sort"
)

const apiUrl = "https://adventofcode.com/2020/leaderboard/private/view/131411.json"

var client *http.Client

func init() {
	secret := os.Getenv("AOC_COOKIE")

	if secret == "" {
		panic(errors.New("missing env variable AOC_COOKIE"))
	}

	jar, err := cookiejar.New(nil)
	if err != nil {
		panic(err)
	}

	u, e := url.Parse("https://adventofcode.com/")
	if e != nil {
		panic(e)
	}
	jar.SetCookies(u, []*http.Cookie{
		&http.Cookie{
			Name:  "session",
			Value: secret,
		},
	})

	client = &http.Client{Jar: jar}
}

type person struct {
	Name   string
	Points int `json:"local_score"`
	Stars  int
}

type leaderboard []person

func (l leaderboard) Len() int      { return len(l) }
func (l leaderboard) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l leaderboard) Less(i, j int) bool {
	p1 := l[i]
	p2 := l[j]
	_, _ = p1, p2
	if l[i].Points != l[j].Points {
		return l[i].Points > l[j].Points
	}
	if l[i].Stars != l[j].Stars {
		return l[i].Stars > l[j].Stars
	}
	return l[i].Name < l[j].Name
}

func (l leaderboard) Diff(other leaderboard) string {
	resp := ""

	newNames := make(map[string]int)
	oldNames := make(map[string]int)

	for _, n := range l {
		newNames[n.Name] = n.Points
	}

	for _, n := range other {
		oldNames[n.Name] = n.Points
	}

	newPeople := ""
	newSolves := ""
	newSolveNum := 0
	for k := range newNames {
		if _, ok := oldNames[k]; !ok {
			newPeople += k + ", "
		} else {
			if oldNames[k] != newNames[k] {
				newSolves += k + ", "
				newSolveNum++
			}
		}
	}

	if newPeople != "" {
		resp += fmt.Sprintf("Hello %südv a csapatban!\n", newPeople)
	}

	if newSolveNum > 0 {
		fmtString := "%s megoldott"
		if newSolveNum > 1 {
			fmtString += "ak"
		}
		fmtString += " egy feladatot!"
		return fmt.Sprintf(fmtString, newSolves[:len(newSolves)-2])
	}

	for i, p := range l {
		if p.Points != other[i].Points || p.Name != other[i].Name || p.Stars != other[i].Stars {
			resp += fmt.Sprintf("%s megoldott egy feladatot!\n", p.Name)
		}
	}

	return ""
}

type response struct {
	Members map[string]person
}

func getLeaderboard() (leaderboard, error) {
	bod, e := client.Get(apiUrl)
	if e != nil {
		return nil, e
	}
	by, e := ioutil.ReadAll(bod.Body)
	if e != nil {
		return nil, e
	}

	var res response
	e = json.Unmarshal(by, &res)
	if e != nil {
		return nil, e
	}

	var lead leaderboard
	for k, v := range res.Members {
		if v.Name == "" {
			v.Name = fmt.Sprintf("Anonymous user #%s", k)
		}
		lead = append(lead, v)
	}

	sort.Sort(lead)

	return lead, nil
}
