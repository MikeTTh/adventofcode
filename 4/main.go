package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

var needed = []string{"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}

func validateField(key, val string) bool {
	switch key {
	case "byr":
		v, e := strconv.Atoi(val)
		if e != nil {
			return false
		}
		return v >= 1920 && v <= 2002
	case "iyr":
		v, e := strconv.Atoi(val)
		if e != nil {
			return false
		}
		return v >= 2010 && v <= 2020
	case "eyr":
		v, e := strconv.Atoi(val)
		if e != nil {
			return false
		}
		return v >= 2020 && v <= 2030
	case "hgt":
		if len(val) < 4 {
			return false
		}

		var v float64
		var s string
		
		r := regexp.MustCompile("(cm|in)$")
		strs := r.Split(val, -1)
		v, e := strconv.ParseFloat(strs[0], 64)
		if e != nil {
			return false
		}
		s = val[len(val)-2:]
		
		switch s {
		case "cm":
			return v >= 150 && v <= 193
		case "in":
			return v >= 59 && v <= 76
		}
	case "hcl":
		m, e := regexp.MatchString("^#([0-9]|[a-f]){6}$", val)
		return e == nil && m
	case "ecl":
		m, e := regexp.MatchString("^(amb|blu|brn|gry|grn|hzl|oth)$", val)
		return e == nil && m
	case "pid":
		m, e := regexp.MatchString("^([0-9]){9}$", val)
		return e == nil && m
	case "cid":
		return true
	}

	return false
}

func find(a []string, b string) int {
	for i, s := range a {
		if s == b {
			return i
		}
	}
	return -1
}

func valid(a []string) bool {
	if len(needed) > len(a) {
		return false
	}
	for _, n := range needed {
		i := find(a, n)
		if i == -1 {
			return false
		}
	}
	return true
}

func valid2(keys, vals []string) bool {
	if len(needed) > len(keys) {
		return false
	}
	for _, n := range needed {
		i := find(keys, n)
		if i == -1 || !validateField(keys[i], vals[i]) {
			return false
		}
	}
	return true
}

func main() {
	by, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}
	str := string(by)
	
	passports := strings.Split(str, "\n\n")

	valids := 0
	valids2 := 0

	for _, p := range passports {
		p = strings.ReplaceAll(p, "\n", " ")
		fields := strings.Split(p, " ")
		keys := make([]string, 0, len(fields))
		vals := make([]string, 0, len(fields))
		for _, f := range fields {
			keyValue := strings.Split(f, ":")
			keys = append(keys, keyValue[0])
			if len(keyValue) > 1 {
				vals = append(vals, keyValue[1])
			} else {
				vals = append(vals, "")
			}
		}
		
		if valid(keys) {
			valids++
		}
		if valid2(keys, vals) {
			valids2++
		}
	}

	fmt.Printf("%d valid passports\n\n", valids)
	fmt.Printf("%d valid passports with strict validation\n", valids2)
}
