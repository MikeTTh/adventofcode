package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	nums := make([]int, 0)
	byt, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}
	strs := strings.Split(string(byt), "\n")
	for _, s := range strs {
		n, e := strconv.Atoi(s)
		if e != nil {
			panic(e)
		}
		nums = append(nums, n)
	}
	
	fmt.Println("1/1")
	for _, x := range nums {
		for _, y := range nums {
			if x+y == 2020 {
				fmt.Printf("%d+%d=2020\n", x, y)
				fmt.Printf("%d*%d=%d\n", x, y, x*y)
				goto end1
			}
		}
	}
	end1:
	
	fmt.Println()
	fmt.Println()

	fmt.Println("1/2")
	for _, x := range nums {
		for _, y := range nums {
			for _, z := range nums {
				if x+y+z == 2020 {
					fmt.Printf("%d+%d+%d=2020\n", x, y, z)
					fmt.Printf("%d*%d*%d=%d\n", x, y, z, x*y*z)
					goto end2
				}
			}
		}
	}
	end2:

}
