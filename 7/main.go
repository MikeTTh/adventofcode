package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

var end = regexp.MustCompile("no other bags")
var r = regexp.MustCompile("[1-9]+ [a-z ]+")

func resolve(what string, where map[string]string) int {
	v := where[what]

	if end.MatchString(v) {
		return 0
	}

	ret := 0

	matches := r.FindAllString(v, -1)
	for _, m := range matches {
		num := 0
		from := 0
		for _, c := range m {
			if c >= '0' && c <= '9' {
				num *= 10
				num += int(c - '0')
				from++
			} else {
				break
			}
		}
		from++
		m = m[from:]
		m = strings.Split(m, " bag")[0]

		ret += num * (resolve(m, where) + 1)
	}

	return ret
}

func main() {
	b, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}

	lines := strings.Split(string(b), "\n")

	const needle = "shiny gold"
	sol := 0

	valid := map[string]bool{
		needle: true,
	}

	bags := make(map[string]string)

	for _, l := range lines {
		parts := strings.Split(l, " bags contain ")
		bags[parts[0]] = parts[1]
	}

	for {
		prevlen := len(valid)
		for toFind := range valid {
			for currentBag, containsBags := range bags {
				if _, ok := valid[currentBag]; ok {
					continue
				}
				if strings.Contains(containsBags, toFind) {
					valid[currentBag] = true
					sol++
				}
			}
		}
		if prevlen == len(valid) {
			break
		}
	}
	fmt.Printf("sol1=%d\n", sol)

	sol2 := resolve(needle, bags)

	fmt.Printf("sol2=%d\n", sol2)

}
