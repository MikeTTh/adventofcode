package main

import (
	"fmt"
	"io/ioutil"
)

type mtrx [][]bool

func parse() mtrx {
	by, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}

	row := 0
	col := 0
	var m mtrx = [][]bool{
		{},
	}

	for _, b := range by {
		switch b {
		case '\n':
			m = append(m, []bool{})
			row++
			col = 0
		case '#':
			m[row] = append(m[row], true)
			col++
		case '.':
			m[row] = append(m[row], false)
			col++
		}
	}

	return m
}

func countTrees(m mtrx, right, down int) int {
	row, col := 0, 0
	trees := 0

	for row < len(m) {
		if m[row][col] {
			trees++
		}
		col += right
		if col >= len(m[row]) {
			col = col - len(m[row])
		}
		row += down
	}

	return trees
}

func main() {
	m := parse()

	pairsToCheck := [][]int{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	}

	mult := 1

	for _, pair := range pairsToCheck {
		right := pair[0]
		down := pair[1]

		trees := countTrees(m, right, down)

		fmt.Printf("%d trees encountered with right %d, down %d\n", trees, right, down)

		mult *= trees
	}

	fmt.Printf("those multiplied together are %d\n", mult)

}
