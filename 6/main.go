package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func chkGroup(group string) int {
	m := make(map[int32]bool)

	for _, s := range group {
		if s != '\n' {
			m[s] = true
		}
	}

	return len(m)
}

func chkGroup2(group string) int {
	m := make(map[int32]int)
	people := 1

	for _, s := range group {
		if s != '\n' {
			m[s] = m[s] + 1
		} else {
			people++
		}
	}

	good := 0

	for _, v := range m {
		if v == people {
			good++
		}
	}

	return good
}

func main() {
	by, e := ioutil.ReadFile("input.txt")
	if e != nil {
		panic(e)
	}

	val := 0
	val2 := 0

	groups := strings.Split(string(by), "\n\n")

	for _, g := range groups {
		val += chkGroup(g)
		val2 += chkGroup2(g)
	}

	fmt.Printf("1. feladat: %d\n", val)
	fmt.Printf("2. feladat: %d\n", val2)
}
